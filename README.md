# Afficheur 7 segments

- Ceci est une afficheur 7 segments qui fonctionne avec des entrées binaire (4) pour sortir tous les chiffres de 0 à 9.
- 
- [x] Uniquement des opérateurs logiques
- [x] Fichiers sources schéma électrique (Proteus)
---
- [x] Entrées binaire (sur 4 bits)
---
> Pour faire fonctionner le relais il suffit de reprendre la trame des codes pour faire fonctionner une led.
---
|  Chiffre numérique  | : Sortie binaire : |
|---------------------|--------------------|
|          0          |        0000        |
|          1          |        0001        |
|          2          |        0010        |
|          3          |        0011        |
|          4          |        0100        |
|          5          |        0101        |
|          6          |        0110        |
|          7          |        0111        |
|          8          |        1000        |
|          9          |        1001        |

<img src = "https://gitlab.com/maxoulfou/afficheur-7-segments/raw/master/images/7_Segments.PNG" title = "schéma" alt = "7 Seg Arduino Schema">


© Maxence Brochier 2018 - 2019